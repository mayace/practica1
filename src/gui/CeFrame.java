package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Label;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.sql.Savepoint;

import java_cup.runtime.lr_parser;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import parser.CeParse;
import parser._1.Parser1;
import parser._1.Scanner1;


public class CeFrame extends JFrame {
	
	CeTabbedPane 	tabbedpane;
	JTextArea		console;

	public CeFrame(){
		this.setJMenuBar(createMenuBar());
		this.setContentPane(createContentPane());
	}

	private Container createContentPane() {
		JPanel			base			=	new JPanel();
		JSplitPane		splitpane1		=	new JSplitPane();
		CeTabbedPane	tabbedpane		=	new CeTabbedPane();
		JTextArea		console			=	new JTextArea();
		JPanel			console_panel	=	new JPanel();
		
		console_panel.setBorder(BorderFactory.createTitledBorder("Console"));
		console_panel.setLayout(new BorderLayout());
		console_panel.add(new JScrollPane(console));
		
		console.setFont(new Font(console.getFont().getName(), 0, 15));
		console.setForeground(Color.red);
		
		
		splitpane1.setBackground(Color.DARK_GRAY);
		splitpane1.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitpane1.setLeftComponent(tabbedpane);
		splitpane1.setRightComponent(console_panel);
		splitpane1.setDividerSize(20);
		splitpane1.setDividerLocation(250);
		splitpane1.setOneTouchExpandable(true);
		
		base.setLayout(new BorderLayout());
		base.add(splitpane1);
		
		this.tabbedpane	=	tabbedpane;
		this.console	=	console;
		return base;
	}

	private JMenuBar createMenuBar() {
		JMenuBar	menubar			=	new JMenuBar();
		JMenu		menu_file		=	new JMenu();
		JMenu		menu_compile	=	new JMenu();
		JMenu		menu_help		=	new JMenu();
		JMenuItem	menu_file_new	=	new JMenuItem();
		JMenuItem	menu_file_open	=	new JMenuItem();
		JMenuItem	menu_file_save	=	new JMenuItem();
		JMenuItem	menu_file_saveAs=	new JMenuItem();
		JMenuItem	menu_file_exit	=	new JMenuItem();
		JMenuItem	menu_compile_1	=	new JMenuItem();
		JMenuItem	menu_compile_2	=	new JMenuItem();
		JMenuItem	menu_compile_3	=	new JMenuItem();
		JMenuItem	menu_compile_4	=	new JMenuItem();
		JMenuItem	menu_compile_5	=	new JMenuItem();
		JMenuItem	menu_compile_6	=	new JMenuItem();
		JMenuItem	menu_help_about	=	new JMenuItem();
		
		menu_file_new.setText("New");
		menu_file_new.setMnemonic('n');
		menu_file_new.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,InputEvent.CTRL_MASK));
		menu_file_new.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				menu_file_new_ActionPerformed(arg0);
			}
		});
		
		menu_file_open.setText("Open");
		menu_file_open.setMnemonic('o');
		menu_file_open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,InputEvent.CTRL_MASK));
		menu_file_open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				menu_file_open_ActionPerformed(arg0);
			}
		});
		
		menu_file_save.setText("Save");
		menu_file_save.setMnemonic('s');
		menu_file_save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_MASK));
		menu_file_save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				menu_file_save_ActionPerformed(arg0);
			}
		});
		
		menu_file_saveAs.setText("Save As");
		menu_file_saveAs.setMnemonic('A');
//		menu_file_saveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,InputEvent.CTRL_MASK));
		menu_file_saveAs.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				menu_file_saveAs_ActionPerformed(arg0);
			}
		});
		
		menu_file_exit.setText("Quit");
		menu_file_exit.setMnemonic('q');
		menu_file_exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,InputEvent.CTRL_MASK));
		menu_file_exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				menu_file_exit_ActionPerformed(arg0);
			}
		});
		
		menu_compile_1.setText("Corchetes anidados");
		menu_compile_1.setMnemonic('1');
		menu_compile_1.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1,InputEvent.CTRL_MASK));
		menu_compile_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(1);
			}
		});
		
		menu_compile_2.setText("Máximo y mínimo de una lista");
		menu_compile_2.setMnemonic('2');
		menu_compile_2.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2,InputEvent.CTRL_MASK));
		menu_compile_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(2);
			}
		});
		
		menu_compile_3.setText("Binario a decimal");
		menu_compile_3.setMnemonic('3');
		menu_compile_3.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3,InputEvent.CTRL_MASK));
		menu_compile_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(3);
			}
		});
		
		menu_compile_4.setText("Declaración de variables");
		menu_compile_4.setMnemonic('4');
		menu_compile_4.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4,InputEvent.CTRL_MASK));
		menu_compile_4.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(4);
			}
		});
		
		menu_compile_5.setText("Operaciones de expresiones aritméticas");
		menu_compile_5.setMnemonic('5');
		menu_compile_5.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5,InputEvent.CTRL_MASK));
		menu_compile_5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(5);
			}
		});
		
		menu_compile_6.setText("Listas anidadas");
		menu_compile_6.setMnemonic('6');
		menu_compile_6.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6,InputEvent.CTRL_MASK));
		menu_compile_6.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
//				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
				compile(6);
			}
		});
		
		menu_help_about.setText("About");
		menu_help_about.setMnemonic('a');
//		menu_help_about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,InputEvent.CTRL_MASK));
		menu_help_about.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JOptionPane.showMessageDialog(null, arg0.getActionCommand());
			}
		});
		
		menu_file.setText("File");
		menu_file.setMnemonic('f');
		menu_file.add(menu_file_new);
		menu_file.add(menu_file_open);
		menu_file.add(menu_file_save);
		menu_file.add(menu_file_saveAs);
		menu_file.add(menu_file_exit);
		
		menu_compile.setText("Compile");
		menu_compile.setMnemonic('c');
		menu_compile.add(menu_compile_1);
		menu_compile.add(menu_compile_2);
		menu_compile.add(menu_compile_3);
		menu_compile.add(menu_compile_4);
		menu_compile.add(menu_compile_5);
		menu_compile.add(menu_compile_6);
		
		menu_help.setText("Help");
		menu_help.setMnemonic('h');
		menu_help.add(menu_help_about);
		
		
		menubar.add(menu_file);
		menubar.add(menu_compile);
		menubar.add(menu_help);
		
		return menubar;
	}

	/***********************************************
	 * fsdfklsd
	 ***********************************************/
	private void doc_new(CeTabbedPane tab_pane,File doc_file){
		//TODO doc_new
		try{
			CeEditor doc_editor = new CeEditor(doc_file);
			int		tab_index	=	tab_pane.getTabCount();
			String 	tab_title	=	"New_"+tab_index;
			if(doc_file!=null){
				tab_title		=	doc_file.getName();
			}
			tab_pane.addTab(tab_title, doc_editor);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	
	private void doc_open() {
		JFileChooser	filechooser		=	new JFileChooser();
		int 			filechooser_r	=	filechooser.showOpenDialog(this);
		
		if(filechooser_r==JFileChooser.APPROVE_OPTION){
			File	filechosser_file	=	filechooser.getSelectedFile();
			doc_new(this.tabbedpane, filechosser_file);
		}
	}
	
	private void doc_save(CeEditor doc_editor){
		try {
			doc_editor.save();
			this.tabbedpane.setTitleAt(getCurrentTabIndex(), doc_editor.get_file().getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void doc_saveAs(CeEditor doc_editor){
		JFileChooser	filechooser		=	new JFileChooser();
		int 			filechooser_r	=	filechooser.showSaveDialog(this);
		
		if(filechooser_r==JFileChooser.APPROVE_OPTION){
			File		filechosser_file	=	filechooser.getSelectedFile();
			doc_editor.set_file(filechosser_file);
			doc_save(doc_editor);
		}
	}
	//
	protected void compile(int i) {
		// TODO compile
		if(getCurrentTabIndex() != -1){
			CeParse	p	=	new CeParse(this.console);
			try {
				p.parse(getCurrentTabEditor().get_editor().getText(), i);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	//
	private int getCurrentTabIndex(){
		return tabbedpane.getSelectedIndex();
	}
	
	private CeEditor getCurrentTabEditor(){
		return	(CeEditor) tabbedpane.getComponentAt(getCurrentTabIndex());
	}
	
	/***********************************************
	 * Actions
	 ***********************************************/
	
	protected void menu_file_new_ActionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		doc_new(this.tabbedpane, null);
	}
	
	protected void menu_file_open_ActionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		doc_open();
	}

	protected void menu_file_save_ActionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		int tab_index =	getCurrentTabIndex();
		if(tab_index>-1){
			CeEditor	tab_editor	=	getCurrentTabEditor();
			
			if(tab_editor.get_file()!=null&&tab_editor.get_file().exists()){
				try {
					tab_editor.save();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
			}
			else{
				menu_file_saveAs_ActionPerformed(arg0);
			}
		}
	}
	
	protected void menu_file_saveAs_ActionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(tabbedpane.getTabCount()>0)
			doc_saveAs(getCurrentTabEditor());
	}

	protected void menu_file_exit_ActionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		System.exit(0);
	}


}
