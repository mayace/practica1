package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class CeEditor extends JPanel {
	JScrollPane	_editor_scrolls;
	JTextArea	_editor;
	File		_file;

	
	/**
	 * 
	 * @param file
	 * @throws IOException 
	 */
	public CeEditor(File file) throws IOException{
		this._file				=	file;
		
		this._editor			=	new JTextArea();
		this._editor_scrolls	=	new JScrollPane();
		
		this._editor.setFont(new Font(this._editor.getFont().getFontName(), 0, 28));
		this._editor_scrolls.setViewportView(this._editor);
		
		this.setLayout(new BorderLayout());
		this.add(_editor_scrolls);
		
		if(file !=null){
			byte[] _file_bytes	=	Files.readAllBytes(file.toPath());
			this._editor.setText(new String(_file_bytes));
		}
			
	}

	public void save() throws IOException{
		Files.write(this._file.toPath(), this._editor.getText().getBytes());
	}
	
	/**
	 * @return the _editor_scrolls
	 */
	public JScrollPane get_editor_scrolls() {
		return _editor_scrolls;
	}

	/**
	 * @param _editor_scrolls the _editor_scrolls to set
	 */
	public void set_editor_scrolls(JScrollPane _editor_scrolls) {
		this._editor_scrolls = _editor_scrolls;
	}

	/**
	 * @return the _editor
	 */
	public JTextArea get_editor() {
		return _editor;
	}

	/**
	 * @param _editor the _editor to set
	 */
	public void set_editor(JTextArea _editor) {
		this._editor = _editor;
	}

	/**
	 * @return the _file
	 */
	public File get_file() {
		return _file;
	}

	/**
	 * @param _file the _file to set
	 */
	public void set_file(File _file) {
		this._file = _file;
	}
	
}
