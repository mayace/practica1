
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sat Aug 18 19:41:58 CST 2012
//----------------------------------------------------

package parser._2;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;
import java.util.ArrayList;
import parser.CeParseAttr;
import dot.CeDotGraph;

/** CUP v0.11a beta 20060608 generated parser.
  * @version Sat Aug 18 19:41:58 CST 2012
  */
public class Parser2 extends java_cup.runtime.lr_parser {

  /** Default constructor. */
  public Parser2() {super();}

  /** Constructor which sets the default scanner. */
  public Parser2(java_cup.runtime.Scanner s) {super(s);}

  /** Constructor which sets the default scanner. */
  public Parser2(java_cup.runtime.Scanner s, java_cup.runtime.SymbolFactory sf) {super(s,sf);}

  /** Production table. */
  protected static final short _production_table[][] = 
    unpackFromStrings(new String[] {
    "\000\014\000\002\011\002\000\002\002\004\000\002\002" +
    "\004\000\002\007\004\000\002\007\003\000\002\010\004" +
    "\000\002\003\005\000\002\003\003\000\002\004\005\000" +
    "\002\005\005\000\002\005\003\000\002\006\003" });

  /** Access to production table. */
  public short[][] production_table() {return _production_table;}

  /** Parse-action table. */
  protected static final short[][] _action_table = 
    unpackFromStrings(new String[] {
    "\000\023\000\004\007\001\001\002\000\004\007\007\001" +
    "\002\000\004\002\006\001\002\000\004\002\uffff\001\002" +
    "\000\004\004\022\001\002\000\006\002\ufffd\007\ufffd\001" +
    "\002\000\006\002\000\007\007\001\002\000\006\005\014" +
    "\006\015\001\002\000\006\005\ufffa\006\ufffa\001\002\000" +
    "\004\007\007\001\002\000\006\002\ufffc\007\ufffc\001\002" +
    "\000\006\005\ufffb\006\ufffb\001\002\000\006\002\ufffe\007" +
    "\ufffe\001\002\000\006\005\ufff7\010\ufff7\001\002\000\006" +
    "\005\024\010\023\001\002\000\006\005\ufff6\010\ufff6\001" +
    "\002\000\006\005\ufff9\006\ufff9\001\002\000\004\004\022" +
    "\001\002\000\006\005\ufff8\010\ufff8\001\002" });

  /** Access to parse-action table. */
  public short[][] action_table() {return _action_table;}

  /** <code>reduce_goto</code> table. */
  protected static final short[][] _reduce_table = 
    unpackFromStrings(new String[] {
    "\000\023\000\006\002\004\011\003\001\001\000\012\003" +
    "\011\004\012\007\010\010\007\001\001\000\002\001\001" +
    "\000\002\001\001\000\006\005\020\006\017\001\001\000" +
    "\002\001\001\000\010\003\011\004\012\010\016\001\001" +
    "\000\002\001\001\000\002\001\001\000\004\004\015\001" +
    "\001\000\002\001\001\000\002\001\001\000\002\001\001" +
    "\000\002\001\001\000\002\001\001\000\002\001\001\000" +
    "\002\001\001\000\004\006\024\001\001\000\002\001\001" +
    "" });

  /** Access to <code>reduce_goto</code> table. */
  public short[][] reduce_table() {return _reduce_table;}

  /** Instance of action encapsulation class. */
  protected CUP$Parser2$actions action_obj;

  /** Action encapsulation object initializer. */
  protected void init_actions()
    {
      action_obj = new CUP$Parser2$actions(this);
    }

  /** Invoke a user supplied parse action. */
  public java_cup.runtime.Symbol do_action(
    int                        act_num,
    java_cup.runtime.lr_parser parser,
    java.util.Stack            stack,
    int                        top)
    throws java.lang.Exception
  {
    /* call code in generated class */
    return action_obj.CUP$Parser2$do_action(act_num, parser, stack, top);
  }

  /** Indicates start state. */
  public int start_state() {return 0;}
  /** Indicates start production. */
  public int start_production() {return 2;}

  /** <code>EOF</code> Symbol index. */
  public int EOF_sym() {return 0;}

  /** <code>error</code> Symbol index. */
  public int error_sym() {return 1;}




	//###########
	//SALIDA...
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	public JTextArea getSalida(){
		return this.salida;
	}
	public void out(String text){
		this.salida.append(text+"\n");
	}
	//###########
	//ERRORES...
    public void report_error(String message, Object info) {
   
        /* Create a StringBuffer called 'm' with the string 'Error' in it. */
        StringBuffer m = new StringBuffer("[Error]");
   
        /* Check if the information passed to the method is the same
           type as the type java_cup.runtime.Symbol. */
        if (info instanceof java_cup.runtime.Symbol) {
            /* Declare a java_cup.runtime.Symbol object 's' with the
               information in the object info that is being typecasted
               as a java_cup.runtime.Symbol object. */
            java_cup.runtime.Symbol s = ((java_cup.runtime.Symbol) info);
   
            /* Check if the line number in the input is greater or
               equal to zero. */
            if (s.left >= 0) {                
                /* Add to the end of the StringBuffer error message
                   the line number of the error in the input. */
                m.append(" in line "+(s.left+1));   
                /* Check if the column number in the input is greater
                   or equal to zero. */
                if (s.right >= 0)                    
                    /* Add to the end of the StringBuffer error message
                       the column number of the error in the input. */
                    m.append(", column "+(s.right+1)+" value <"+s.value+">");
            }
        }
   
        /* Add to the end of the StringBuffer error message created in
           this method the message that was passed into this method. */
        m.append(" : "+message);
   
        /* Print the contents of the StringBuffer 'm', which contains
           an error message, out on a line. */
        //System.err.println(m);
		salida.append(m+"\n");
    }
   
    public void report_fatal_error(String message, Object info) {
        report_error(message, info);
    }


}

/** Cup generated class to encapsulate user supplied action code.*/
class CUP$Parser2$actions {



	//////////////////////////
	//varios...
	void println(String msg){
		parser.out(msg);
	}
	//////////////////////////
	//errores
	void error(String msg, Object info){
		parser.report_error(msg,info);
	}
	CeDotGraph	g1	=	new CeDotGraph("g21","graphs/g21.dot");
	CeDotGraph	g2	=	new CeDotGraph("g22","graphs/g22.dot");
	
	String getNextItemId(){
		return g2.getNextItemId();
	}
	String addItem(String label1,String label2){
		g1.addItem(label1);
		return g2.addItem(label2);
	}
	String addItem(String label){
		g1.addItem(label);
		return g2.addItem(label);
	}
	String addAssn(String from,String to){
		g1.addAssn(from,to);
		return g2.addAssn(from,to);
	}
	String addAssn2(String from,String to){
		return g2.addAssn(from,to);
	}
	String addAssn(String from,String to1,String to2){
		g1.addAssn(from,to1);
		return g2.addAssn(from,to2);
	}
	String addOther(String other){
		g1.addOther(other);
		return g2.addOther(other);
	}
	void createGraphs() throws java.io.IOException{
		g1.getGraphImg();
		g2.getGraphImg();
		println(String.format("Files created on /%S dir...",g1.getDotfile().getParent()));
	}

  private final Parser2 parser;

  /** Constructor */
  CUP$Parser2$actions(Parser2 parser) {
    this.parser = parser;
  }

  /** Method with the actual generated action code. */
  public final java_cup.runtime.Symbol CUP$Parser2$do_action(
    int                        CUP$Parser2$act_num,
    java_cup.runtime.lr_parser CUP$Parser2$parser,
    java.util.Stack            CUP$Parser2$stack,
    int                        CUP$Parser2$top)
    throws java.lang.Exception
    {
      /* Symbol object for return from actions */
      java_cup.runtime.Symbol CUP$Parser2$result;

      /* select the action based on the action number */
      switch (CUP$Parser2$act_num)
        {
          /*. . . . . . . . . . . . . . . . . . . .*/
          case 11: // int ::= INT 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		String x = (String)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						CeParseAttr	attr	=	new CeParseAttr();
						
						
						String	item_id	=	addItem(x);
						
						attr.set("item_id",item_id);
						attr.set("int",x);
						
						RESULT=attr;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("int",4, ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 10: // int_list ::= int 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						String	item_id		=	addItem("int");
						String	item_id0	=	x.getString("item_id");
						
						addAssn(item_id,item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,x.toString("\\n","item_id")));
						
						x.set("item_id",item_id);
						//
						x.set("M",x.get("int"));	//M=MAYOR
						x.set("m",x.get("int"));	//m=menor
						
						RESULT=x;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("int_list",3, ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 9: // int_list ::= int_list SEMI int 
            {
              CeParseAttr RESULT =null;
		int lleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).left;
		int lright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).right;
		CeParseAttr l = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).value;
		int ileft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int iright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr i = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
					
						//g
						String item_id	=	addItem("<f0>int_list|,|<f2>int");
						String item_id0	=	l.getString("item_id");
						String item_id2	=	i.getString("item_id");
						
						
						addAssn(item_id+":f0",item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,l.toString("\\n","item_id")));
						addAssn(item_id+":f2",item_id2,String.format("%s [dir=\"back\",label=\"%s\"]",item_id2,i.toString("\\n","item_id")));
						l.set("item_id",item_id);
						//
						int	M	=	l.getInteger("M");
						int	m	=	l.getInteger("m");
						int ii	=	i.getInteger("int");
						
						if(ii>M)
							l.set("M",ii);
						if(ii<m)
							l.set("m",ii);
						
						
						RESULT=l;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("int_list",3, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 8: // array ::= CORCHETE1 int_list CORCHETE2 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).value;
		
						//g
						String item_id	=	addItem("[|<f1>int_list|]");
						String item_id1	=	x.getString("item_id");
						
						x.set("item_id",item_id);
						
						addAssn(item_id+":f1",item_id1,String.format("%s [dir=\"back\",label=\"%s\"]",item_id1,x.toString("\\n","item_id")));
						RESULT=x;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("array",2, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 7: // array_list ::= array 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						String	item_id		=	addItem("array");
						String	item_id0	=	x.getString("item_id");
						
						addAssn(item_id,item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,x.toString("\\n","item_id")));
						
						x.set("item_id",item_id);
						//
						x.set("Ml",x.get("M"));				//M list
						x.set("ml","["+x.get("m")+"]");		//m list
						
						RESULT=x;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("array_list",1, ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 6: // array_list ::= array_list SEMI array 
            {
              CeParseAttr RESULT =null;
		int lleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).left;
		int lright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).right;
		CeParseAttr l = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)).value;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						//g
						String item_id	=	addItem("<f0>array_list|,|<f2>array");
						String item_id0	=	l.getString("item_id");
						String item_id2	=	x.getString("item_id");
						
						
						addAssn(item_id+":f0",item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,l.toString("\\n","item_id")));
						addAssn(item_id+":f2",item_id2,String.format("%s [dir=\"back\",label=\"%s\"]",item_id2,x.toString("\\n","item_id")));
						l.set("item_id",item_id);
						//
						l.set("Ml",l.getString("Ml")+","+x.getString("M"));
						l.set("ml",l.getString("ml")+",["+x.getString("m")+"]");
						
						RESULT=l;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("array_list",1, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-2)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 5: // item ::= array_list SEMIC 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).value;
		
						println(x.getString("Ml"));
						println(x.getString("ml"));
						//g
						String item_id	=	addItem("<f0>array_list|;");
						String item_id0	=	x.getString("item_id");
						
						addAssn(item_id+":f0",item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,x.toString("\\n","item_id")));
						
						x.clear();
						x.set("item_id",item_id);
						
						RESULT	=	x;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("item",6, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 4: // list ::= item 
            {
              CeParseAttr RESULT =null;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						//g
						String	item_id		=	addItem("array");
						String	item_id0	=	x.getString("item_id");
						
						addAssn(item_id,item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,x.toString("\\n","item_id")));
						
						x.set("item_id",item_id);
						RESULT=x;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("list",5, ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 3: // list ::= list item 
            {
              CeParseAttr RESULT =null;
		int lleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).left;
		int lright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).right;
		CeParseAttr l = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).value;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						//g
						String item_id	=	addItem("<f0>list|<f1>item");
						String item_id0	=	l.getString("item_id");
						String item_id1	=	x.getString("item_id");
						
						addAssn(item_id+":f0",item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,l.toString("\\n","item_id")));
						addAssn(item_id+":f1",item_id1,String.format("%s [dir=\"back\",label=\"%s\"]",item_id1,x.toString("\\n","item_id")));
						l.set("item_id",item_id);
						RESULT=l;
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("list",5, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 2: // $START ::= begin EOF 
            {
              Object RESULT =null;
		int start_valleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).left;
		int start_valright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).right;
		CeParseAttr start_val = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).value;
		RESULT = start_val;
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("$START",0, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          /* ACCEPT */
          CUP$Parser2$parser.done_parsing();
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 1: // begin ::= NT$0 list 
            {
              CeParseAttr RESULT =null;
              // propagate RESULT from NT$0
                RESULT = (CeParseAttr) ((java_cup.runtime.Symbol) CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)).value;
		int xleft = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).left;
		int xright = ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()).right;
		CeParseAttr x = (CeParseAttr)((java_cup.runtime.Symbol) CUP$Parser2$stack.peek()).value;
		
						String item_id0	=	x.getString("item_id");
						String item_id	=	addItem("list");
						String begin_id	=	addItem("begin");
						
						addAssn(item_id,item_id0,String.format("%s [dir=\"back\",label=\"%s\"]",item_id0,x.toString("\\n","item_id")));
						addAssn(begin_id,item_id,item_id+" [dir=\"back\"]");
						addOther("node [shape=record]");
						createGraphs();
						println(".....................");
					
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("begin",0, ((java_cup.runtime.Symbol)CUP$Parser2$stack.elementAt(CUP$Parser2$top-1)), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /*. . . . . . . . . . . . . . . . . . . .*/
          case 0: // NT$0 ::= 
            {
              CeParseAttr RESULT =null;
println("parse2...............");
              CUP$Parser2$result = parser.getSymbolFactory().newSymbol("NT$0",7, ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), ((java_cup.runtime.Symbol)CUP$Parser2$stack.peek()), RESULT);
            }
          return CUP$Parser2$result;

          /* . . . . . .*/
          default:
            throw new Exception(
               "Invalid action number found in internal parse table");

        }
    }
}

