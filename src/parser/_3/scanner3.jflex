package parser._3;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class Scanner3
%public
%line
%column
%cup


%{
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	public JTextArea getSalida(){
		return this.salida;
	}
	public void out(String text){
		this.salida.append(text+"\n");
	}
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
// LETRA		=[a-z�A-Z�]
// DIGIT		=[0-9]

// INT			="-"?{DIGIT}+
SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 


%%

<YYINITIAL>{
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	";"				{return symbol(Sym3.SEMIC,yytext());}
	"0"				{return symbol(Sym3._0,yytext());}
	"1"				{return symbol(Sym3._1,yytext());}
}

<<EOF>>				{return symbol(Sym3.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}