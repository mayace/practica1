package parser._1;

import java_cup.runtime.Symbol;
import javax.swing.JTextArea;

%%

%class Scanner1
%public
%line
%column
%cup


%{
	JTextArea salida=new JTextArea();
	public void setSalida(JTextArea salida){
		this.salida=salida;
	}
	public JTextArea getSalida(){
		return this.salida;
	}
	public void out(String text){
		this.salida.append(text+"\n");
	}
    /* To create a new java_cup.runtime.Symbol with information about
       the current token, the token will have no value in this
       case. */
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    /* Also creates a new java_cup.runtime.Symbol with information
       about the current token, but this object has a value. */
    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }
%}

NEWLINE		=\n|\r|\r\n
ESPACIO     =[ \t]|{NEWLINE}
// LETRA		=[a-z�A-Z�]
DIGIT		=[0-9]

INT			="-"?{DIGIT}+
SIMPLE_C	="//"[^\r\n]*{NEWLINE}
MULTI_C		="/*"[^\*\/]*"*/" 


%%

<YYINITIAL>{
	{ESPACIO}		{/*nada*/}
	{SIMPLE_C}		{/*COMENTARIO*/}
	{MULTI_C}		{/*COMENTARIO*/}
	{INT}			{return symbol(Sym1.INT,yytext());}
	","				{return symbol(Sym1.SEMI,yytext());}
	";"				{return symbol(Sym1.SEMIC,yytext());}
	"["				{return symbol(Sym1.CORCHETE1,yytext());}
	"]"				{return symbol(Sym1.CORCHETE2,yytext());}
}

<<EOF>>				{return symbol(Sym1.EOF,yytext());}

[^]		{salida.append("[Error] Illegal character <"+yytext()+"> at line "+(yyline+1)+" column " +(yycolumn+1)+"\n");}