package parser;

import java.io.StringReader;

import javax.swing.JTextArea;

import parser._1.Parser1;
import parser._1.Scanner1;
import parser._2.Parser2;
import parser._2.Scanner2;
import parser._3.Parser3;
import parser._3.Scanner3;
import parser._4.Parser4;
import parser._4.Scanner4;
import parser._5.Parser5;
import parser._5.Scanner5;
import parser._6.Parser6;
import parser._6.Scanner6;


public class CeParse {
	
	JTextArea	console;
	
	public CeParse(JTextArea console) {
		// TODO Auto-generated constructor stub
		this.console	=	console;
	}
	
	public void parse(String text,int i) throws Exception{
		if(text!=null&&!text.trim().isEmpty()){
			StringReader	input			=	new StringReader(text); 
			String			dotfile_path	=	"dotfile.dot";
			switch(i){
				case 1:
					Scanner1	s1	=	new Scanner1(input);
					Parser1		p1	=	new Parser1(s1);
					s1.setSalida(console);
					
					p1.setSalida(console);
					p1.parse();
					break;
				case 2:
					Scanner2	s2	=	new Scanner2(input);
					Parser2		p2	=	new Parser2(s2);
					s2.setSalida(console);
					p2.setSalida(console);
					p2.parse();
					break;
				case 3:
					Scanner3	s3	=	new Scanner3(input);
					Parser3		p3	=	new Parser3(s3);
					s3.setSalida(console);
					p3.setSalida(console);
					p3.parse();
					break;
				case 4:
					Scanner4	s4	=	new Scanner4(input);
					Parser4		p4	=	new Parser4(s4);
					s4.setSalida(console);
					p4.setSalida(console);
					p4.parse();
					break;
				case 5:
					Scanner5	s5	=	new Scanner5(input);
					Parser5		p5	=	new Parser5(s5);
					s5.setSalida(console);
					p5.setSalida(console);
					p5.parse();
					break;
				case 6:
					Scanner6	s6	=	new Scanner6(input);
					Parser6		p6	=	new Parser6(s6);
					s6.setSalida(console);
					p6.setSalida(console);
					p6.parse();
					break;
			}
		}
	}

	
	/**
	 * @return the console
	 */
	public JTextArea getConsole() {
		return console;
	}

	/**
	 * @param console the console to set
	 */
	public void setConsole(JTextArea console) {
		this.console = console;
	}
	
}
